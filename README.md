# Introduction
As developers we need to make our work easier. Working with git from the bash shell can become pretty verbose. Here's a collection of scripts that aims to ease that.
# Cloning
Anyone with read access to this repository can clone this collection of scripts. If you wish to submit improvements, please consider creating a pull request and I'll merge it if it doesn't break stuff.
# Installation
Before you do anything, you need to clone this repository. Afterwards, just add the following line to your ~/.profile file:
```bash
. /path/to/aliases.sh
```
where */path/to/aliases.sh* is the path to where you've cloned the aliases.sh file hosted in this repository.
# Uninstall
Remove that line from your ~/.profile file.