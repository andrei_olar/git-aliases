#!/bin/bash

function checkout_new()
{
	git checkout -b $1
}

function trigger_ci()
{
	git commit --allow-empty --message "Trigger CI"
	git push
}

function post_review()
{
	local ref="refs/for/"
	local commit="HEAD"
	local branch="master"
	while [ $# -gt 0 ]; do
		key=$1
		shift
		case $key in
			-d|--draft)
				ref="refs/drafts/"
				;;
			-c|--change)
				commit="$1"
				shift
				;;
			-b|--branch)
				branch="$1"
				shift
				;;
			*)
				echo "Unknown argument '$1'"
				;;
		esac
	done
	git push origin $commit:$ref$branch
}

function cdsrc()
{
	local src="${SRC:-$HOME}"
	cd $SRC
}

function up()
{
	local branch="$( git rev-parse --abbrev-ref HEAD)"
	if [[ "zHEAD" == "z$branch" ]]
	then
		echo "Please checkout a branch"
		exit 1
	fi

	if (git show-ref --quiet --verify -- "refs/remotes/origin/$branch")
	then
		git push
	else
		git push -u origin $branch:$branch
	fi
}

function keep_branches()
{
	local branch_name=${1:-"master"}
	local deleted_branches=$( git branch --format '%(refname)' | grep -v -E "${branch_name}" )
	printf "Will remove the following branches:\n--\n%s\n--\nContinue? [Y/N] " ${deleted_branches}
	read choice
	case "${choice}" in
		y|Y|yes|Yes)
			git branch -D $( git branch | grep -v "${branch_name}" )
		;;
	esac
}

alias gs='git status '
alias gss='git status --short | grep -v "??"'
alias ga='git add '
alias gb='git branch '
alias gbl='git branch -l'
alias gblr='git branch -l -r'
alias gbla='git branch -l -a'
alias gbrm='git branch -D'
alias gbkeep='keep_branches'
alias gc='git commit'
alias gca='git commit --amend'
alias gd='git diff'
alias gds='git diff --staged'
alias stat='git diff --stat'
alias sstat='git diff --staged --stat'
alias gco='git checkout '
alias gconew='checkout_new'
alias up='up'
alias pr='post_review'
alias gp='git pull'
alias gpr='git pull --rebase'
alias grb='git rebase'
alias grba='git rebase --abort'
alias grbc='git rebase --continue'
alias gk='gitk --all&'
alias gx='gitx --all'
alias gcp='git cherry-pick -x'
alias grm='git rm'
alias gl='git log --oneline --decorate'
alias glp='git log -p'
alias glg="git log --graph --abbrev-commit --decorate --date=relative --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
alias grst='git reset'
alias gfap='git fetch --all --prune'
alias gfapmom='git fetch --all --prune && git merge origin master'
alias tci='trigger_ci'
